#pragma once

#ifdef WIN32
#define BREAKPOINT __debugbreak()
#else
#define BREAKPOINT
#endif

#define ASSERT(condition, explanation) if(!(condition)) { BREAKPOINT; }
#define FATAL_ASSERT(condition, explanation) if(!(condition)) { BREAKPOINT; abort(); }

#define VK_ASSERT(expr, assert_msg)             \
    switch(vk::Result{ expr }) {                \
    case vk::Result::eSuccess:                  \
    break;                                      \
    default:                                    \
        throw std::runtime_error{ assert_msg }; \
    }
