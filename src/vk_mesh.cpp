#include "vk_mesh.h"
#include <tiny_obj_loader.h>
#include <iostream>

const vk::PipelineVertexInputStateCreateInfo Vertex::VertexInputInfo{ {}, BindingDescription, AttributeDescriptions };

const vk::VertexInputBindingDescription Vertex::BindingDescription{ 0, sizeof(Vertex), vk::VertexInputRate::eVertex };

const std::array<vk::VertexInputAttributeDescription, 4> Vertex::AttributeDescriptions{
	vk::VertexInputAttributeDescription{ 0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, position) },
	vk::VertexInputAttributeDescription{ 1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal) },
	vk::VertexInputAttributeDescription{ 2, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, color) },
    vk::VertexInputAttributeDescription{ 3, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, uv) },
};

void Mesh::load_model(const char* path) {
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, path)) {
        std::cerr << "tinyobj loader errors:\n" << err << std::endl;
        throw std::runtime_error{ "unable to load model file." };
    }

    if (!warn.empty())
        std::cerr << "tinyobj loader warnings:\n" << warn << std::endl;

    //std::unordered_map<Vertex, uint32_t> vertex_to_index;

    for (const tinyobj::shape_t& shape : shapes) {
        for (const tinyobj::index_t& index : shape.mesh.indices) {
            Vertex vertex{ glm::vec3{ attrib.vertices[3 * index.vertex_index + 0], attrib.vertices[3 * index.vertex_index + 1], attrib.vertices[3 * index.vertex_index + 2] },
                glm::vec3{ attrib.normals[3 * index.normal_index + 0], attrib.normals[3 * index.normal_index + 1], attrib.normals[3 * index.normal_index + 2] },
                glm::vec3{ attrib.normals[3 * index.normal_index + 0], attrib.normals[3 * index.normal_index + 1], attrib.normals[3 * index.normal_index + 2] },
                glm::vec2{ attrib.texcoords[2 * index.texcoord_index + 0], 1.0f - attrib.texcoords[2 * index.texcoord_index + 1] },
            };

            //auto it_find = vertex_to_index.find(vertex);
            //if (it_find == vertex_to_index.end()) {
            //    it_find = vertex_to_index.insert(it_find, std::make_pair(vertex, (uint32_t) _vertices.size()));
                _vertices.push_back(std::move(vertex));
            //}
            //indices.push_back(it_find->second);
        }
    }
}
