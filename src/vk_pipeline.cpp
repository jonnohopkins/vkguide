#include "vk_pipeline.h"

vk::Pipeline VkPipelineBuilder::build(vk::Device device, vk::RenderPass renderpass) {
    const vk::PipelineViewportStateCreateInfo viewport_info{ {}, _viewport, _scissor };
    const vk::PipelineColorBlendStateCreateInfo color_blending_info{ {}, VK_FALSE, vk::LogicOp::eCopy, _colorBlendAttachment };

    const vk::GraphicsPipelineCreateInfo graphics_pipeline_info{ {}, _shaderStages,
        &_vertexInputInfo, &_inputAssembly, nullptr, &viewport_info, &_rasterizer, &_multisampling, & _depthStencil, &color_blending_info, nullptr,
        _pipelineLayout, renderpass, 0,
        VK_NULL_HANDLE, 0
    };

    auto pipeline_result = device.createGraphicsPipeline(VK_NULL_HANDLE, graphics_pipeline_info);
    switch (pipeline_result.result) {
    case vk::Result::eSuccess:
        return pipeline_result.value;
        break;
    default:
        throw std::runtime_error{ "failed to create graphics pipeline." };
        return VK_NULL_HANDLE;
    }
}
