#pragma once

#include <vulkan/vulkan.hpp>
#include <shaderc/shaderc.hpp>

vk::ShaderModule compile_shader_module(vk::Device device, const char* path, shaderc_shader_kind shader_kind);
