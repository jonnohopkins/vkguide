#include "vk_engine.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "vk_types.h"
#include "vk_initializers.h"
#include "util.h"
#include "shader_compile.h"
#include "vk_pipeline.h"
#include "vk_textures.h"

static void framebuffer_resize_cb(GLFWwindow* window, int width, int height) {
    VkEngine* engine = reinterpret_cast<VkEngine*>(glfwGetWindowUserPointer(window));
    engine->_lifecycleState.has_graphics = (width != 0 && height != 0);
    engine->_isSwapchainDirty = true;
}

static void window_focus_cb(GLFWwindow* window, int focused) {
    VkEngine* engine = reinterpret_cast<VkEngine*>(glfwGetWindowUserPointer(window));
    engine->_lifecycleState.has_input_focus = (focused == GLFW_TRUE);
}

static void window_key_cb(GLFWwindow* window, int key, int scancode, int action, int mods) {
    VkEngine* engine = reinterpret_cast<VkEngine*>(glfwGetWindowUserPointer(window));

    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_ESCAPE) {
            glfwSetWindowShouldClose(window, true);
        }
    }
}

void VkEngine::init() {
    FATAL_ASSERT(glfwInit(), "glfw failed to initialize.");

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // don't initialize an OpenGL context

    GLFWmonitor* primary_monitor = glfwGetPrimaryMonitor();

    int work_xpos, work_ypos, work_width, work_height;
    glfwGetMonitorWorkarea(primary_monitor, &work_xpos, &work_ypos, &work_width, &work_height);

    _window = glfwCreateWindow(work_width, work_height, "Vulkan Guide", nullptr, nullptr);
    if (!_window) {
        glfwTerminate();
        throw std::runtime_error{ "Failed to create GLFW window" };
    }

    glfwSetWindowUserPointer(_window, this);
    glfwSetFramebufferSizeCallback(_window, framebuffer_resize_cb);
    glfwSetWindowFocusCallback(_window, window_focus_cb);
    glfwSetKeyCallback(_window, window_key_cb);

    _lifecycleState.has_input_focus = (glfwGetWindowAttrib(_window, GLFW_FOCUSED) == GLFW_TRUE);

    init_vulkan();
    init_descriptors();
    load_textures();
    load_meshes();
    init_swapchain();
    init_scene();
}

void VkEngine::init_vulkan() {
    _vkbInstance = vkb::InstanceBuilder{}.set_app_name("VK Guide")
        .request_validation_layers(true)
        .require_api_version(1, 1, 0)
        .use_default_debug_messenger()
        .build().value();

    _instance = _vkbInstance.instance;

    if (glfwCreateWindowSurface(_instance, _window, nullptr, &(VkSurfaceKHR&)_surface) != VK_SUCCESS) {
        throw std::runtime_error("failed to create window surface.");
    }

    vkb::PhysicalDevice vkb_physical_device = vkb::PhysicalDeviceSelector{ _vkbInstance }.set_minimum_version(1, 1)
        .set_surface(_surface)
        .select().value();
    vk::PhysicalDeviceShaderDrawParameterFeatures shader_draw_parameter_features{ VK_TRUE };
    _vkbDevice = vkb::DeviceBuilder{ vkb_physical_device }.add_pNext(&shader_draw_parameter_features).build().value();

    _physicalDevice = vkb_physical_device.physical_device;
    _device = _vkbDevice.device;

    _graphicsQueue = _vkbDevice.get_queue(vkb::QueueType::graphics).value();
    _graphicsQueueFamily = _vkbDevice.get_queue_index(vkb::QueueType::graphics).value();

    VmaAllocatorCreateInfo allocator_info{};
    allocator_info.physicalDevice = _physicalDevice;
    allocator_info.device = _device;
    allocator_info.instance = _instance;
    vmaCreateAllocator(&allocator_info, &_allocator);

    _frameData.resize(max_frames_in_flight);
    for (auto& frame_datum : _frameData) {
        frame_datum._presentSemaphore = _device.createSemaphore(vk::SemaphoreCreateInfo{});
        frame_datum._renderSemaphore = _device.createSemaphore(vk::SemaphoreCreateInfo{});
        frame_datum._renderFence = _device.createFence(vk::FenceCreateInfo{ vk::FenceCreateFlagBits::eSignaled });

        frame_datum._commandPool = _device.createCommandPool(vk::CommandPoolCreateInfo{ vk::CommandPoolCreateFlagBits::eResetCommandBuffer, _graphicsQueueFamily });
        frame_datum._mainCommandBuffer = _device.allocateCommandBuffers(vk::CommandBufferAllocateInfo{ frame_datum._commandPool, vk::CommandBufferLevel::ePrimary, 1 }).front();

        frame_datum._uboCamera = create_buffer(sizeof(UboCameraData), vk::BufferUsageFlagBits::eUniformBuffer, VMA_MEMORY_USAGE_CPU_TO_GPU, {}, {});
        frame_datum._ssboObjects = create_buffer(sizeof(SsboObjectData) * max_objects, vk::BufferUsageFlagBits::eStorageBuffer, VMA_MEMORY_USAGE_CPU_TO_GPU, {}, {});
    }

    _physicalDeviceProperties = _vkbDevice.physical_device.properties;

    const size_t scene_data_buffer_size = max_frames_in_flight * pad_uniform_buffer_size(sizeof(UboSceneData));
    _sceneDataBuffer = create_buffer(scene_data_buffer_size, vk::BufferUsageFlagBits::eUniformBuffer, VMA_MEMORY_USAGE_CPU_TO_GPU, {}, {});

    _uploadContext._uploadFence = _device.createFence(vk::FenceCreateInfo{});
    _uploadContext._commandPool = _device.createCommandPool(vk::CommandPoolCreateInfo{ {}, _graphicsQueueFamily });
    _uploadContext._commandBuffer = _device.allocateCommandBuffers(vk::CommandBufferAllocateInfo{ _uploadContext._commandPool, vk::CommandBufferLevel::ePrimary, 1 }).front();
}

void VkEngine::init_descriptors() {
    const std::vector<vk::DescriptorPoolSize> global_pool_sizes{
        vk::DescriptorPoolSize{ vk::DescriptorType::eUniformBuffer, num_uniform_buffers },
        vk::DescriptorPoolSize{ vk::DescriptorType::eUniformBufferDynamic, num_uniform_dynamic_buffers },
        vk::DescriptorPoolSize{ vk::DescriptorType::eStorageBuffer, num_storage_buffers },
    };
    _globalDescriptorPool = _device.createDescriptorPool(vk::DescriptorPoolCreateInfo{ {}, max_global_descriptor_sets, global_pool_sizes });

    {
        const std::array<vk::DescriptorSetLayoutBinding, 2> global_bindings = {
            vkinit::descriptorset_layout_binding(vk::DescriptorType::eUniformBuffer, vk::ShaderStageFlagBits::eVertex, ubo_camera_binding),
            vkinit::descriptorset_layout_binding(vk::DescriptorType::eUniformBufferDynamic, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, ubo_scene_binding),
        };
        _globalDescriptorSetLayout = _device.createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo{ {}, global_bindings });

        const std::vector<vk::DescriptorSetLayout> layouts(max_frames_in_flight, _globalDescriptorSetLayout);
        _frameGlobalDescriptors = _device.allocateDescriptorSets(vk::DescriptorSetAllocateInfo{ _globalDescriptorPool, layouts });
    }

    {
        const std::array<vk::DescriptorSetLayoutBinding, 1> object_bindings = {
            vkinit::descriptorset_layout_binding(vk::DescriptorType::eStorageBuffer, vk::ShaderStageFlagBits::eVertex, ssbo_object_binding),
        };
        _objectDescriptorSetLayout = _device.createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo{ {}, object_bindings });

        const std::vector<vk::DescriptorSetLayout> layouts(max_frames_in_flight, _objectDescriptorSetLayout);
        _frameObjectDescriptors = _device.allocateDescriptorSets(vk::DescriptorSetAllocateInfo{ _globalDescriptorPool, layouts });
    }

    for (size_t i = 0; i < max_frames_in_flight; ++i) {
        const vk::DescriptorBufferInfo camera_buffer_info{ _frameData[i]._uboCamera._buffer, 0, sizeof(UboCameraData) };
        const vk::DescriptorBufferInfo scene_buffer_info{ _sceneDataBuffer._buffer, 0, sizeof(UboSceneData)};
        const vk::DescriptorBufferInfo object_buffer_info{ _frameData[i]._ssboObjects._buffer, 0, sizeof(SsboObjectData) * max_objects};
        const std::array<vk::WriteDescriptorSet, 3> buffer_descriptor_writes{
            vkinit::write_descriptor_set(vk::DescriptorType::eUniformBuffer, _frameGlobalDescriptors[i], &camera_buffer_info, ubo_camera_binding),
            vkinit::write_descriptor_set(vk::DescriptorType::eUniformBufferDynamic, _frameGlobalDescriptors[i], &scene_buffer_info, ubo_scene_binding),
            vkinit::write_descriptor_set(vk::DescriptorType::eStorageBuffer, _frameObjectDescriptors[i], &object_buffer_info, ssbo_object_binding),
        };
        _device.updateDescriptorSets(buffer_descriptor_writes, nullptr);
    }

    {
        const std::array<vk::DescriptorSetLayoutBinding, 1> material_bindings = {
            vkinit::descriptorset_layout_binding(vk::DescriptorType::eCombinedImageSampler, vk::ShaderStageFlagBits::eFragment, combined_sampler_binding),
        };
        _singleTextureSetLayout = _device.createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo{ {}, material_bindings });
    }

    nearest_sampler = _device.createSampler(vkinit::sampler_create_info(vk::Filter::eNearest, vk::SamplerAddressMode::eRepeat));
}

void VkEngine::init_swapchain() {
    ASSERT(!_swapchain, "swapchain should be uninitialized.");

    int pixel_width, pixel_height;
    glfwGetFramebufferSize(_window, &pixel_width, &pixel_height);
    _windowExtent.width = pixel_width;
    _windowExtent.height = pixel_height;

    _vkbSwapchain = vkb::SwapchainBuilder{ _vkbDevice, _surface }.use_default_format_selection()
        .set_desired_present_mode(VkPresentModeKHR::VK_PRESENT_MODE_FIFO_KHR)
        .set_desired_extent(_windowExtent.width, _windowExtent.height)
        .build().value();

    _swapchain = _vkbSwapchain.swapchain;
    _swapchainImages = _device.getSwapchainImagesKHR(_swapchain);
    _swapchainImageFormat = vk::Format{ _vkbSwapchain.image_format };
    _swapchainImageViews.resize(_swapchainImages.size());
    for (size_t i = 0; i < _swapchainImages.size(); ++i) {
        _swapchainImageViews[i] = (_device.createImageView(vk::ImageViewCreateInfo{
            {}, _swapchainImages[i], vk::ImageViewType::e2D, _swapchainImageFormat, {}, vk::ImageSubresourceRange{
                vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1
            }
            }));
    }

    // https://vulkan.gpuinfo.org/listoptimaltilingformats.php?platform=windows
    // D32_SFLOAT, D32_SFLOAT_S8_UINT is 100% supported on Windows, macOS, iOS and close to 100% on Linux.
    // D16_UNORM, D24_UNORM_S8_UINT and X8_D24_UNORM_PACK32 are close to 100% supported on Android.
    _depthFormat = vk::Format::eD32Sfloat;

    const vk::Extent3D depth_image_extent{ _windowExtent.width, _windowExtent.height, 1 };
    const auto depth_image_info = (VkImageCreateInfo&) vkinit::image_create_info(_depthFormat, depth_image_extent, 1, vk::SampleCountFlagBits::e1, vk::ImageUsageFlagBits::eDepthStencilAttachment);

    VmaAllocationCreateInfo depth_image_alloc_info{};
    depth_image_alloc_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    depth_image_alloc_info.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    vmaCreateImage(_allocator, &depth_image_info, &depth_image_alloc_info, &(VkImage&) _depthImage._image, &_depthImage._allocation, nullptr);
    _depthImageView = _device.createImageView(vkinit::imageview_create_info(_depthFormat, _depthImage._image, vk::ImageAspectFlagBits::eDepth));

    _lifecycleState.has_graphics = (_windowExtent.width != 0 && _windowExtent.height != 0);

    init_default_renderpass();
    init_framebuffers();
    init_pipelines();
}

void VkEngine::init_default_renderpass() {
    const vk::AttachmentDescription color_attachment{ {}, _swapchainImageFormat, vk::SampleCountFlagBits::e1,
        vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore,
        vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
        vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR
    };
    const vk::AttachmentReference color_attachment_ref{ 0, vk::ImageLayout::eColorAttachmentOptimal };

    const vk::AttachmentDescription depth_attachment{ {}, _depthFormat, vk::SampleCountFlagBits::e1,
        vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare,
        vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare,
        vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal
    };
    const vk::AttachmentReference depth_attachment_ref{ 1, vk::ImageLayout::eDepthStencilAttachmentOptimal };

    const vk::SubpassDescription subpass_desc{ {}, vk::PipelineBindPoint::eGraphics, nullptr, color_attachment_ref, nullptr, &depth_attachment_ref, nullptr };

    const vk::SubpassDependency color_dependency{ VK_SUBPASS_EXTERNAL, 0,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        {}, vk::AccessFlagBits::eColorAttachmentWrite,
        {}
    };

    const vk::SubpassDependency depth_dependency{ VK_SUBPASS_EXTERNAL, 0,
        vk::PipelineStageFlagBits::eEarlyFragmentTests | vk::PipelineStageFlagBits::eLateFragmentTests,
        vk::PipelineStageFlagBits::eEarlyFragmentTests | vk::PipelineStageFlagBits::eLateFragmentTests,
        {}, vk::AccessFlagBits::eDepthStencilAttachmentWrite,
        {}
    };

    const std::array<vk::AttachmentDescription, 2> attachments{ color_attachment, depth_attachment };
    const std::array<vk::SubpassDependency, 2> dependencies{ color_dependency, depth_dependency };
    _renderPass = _device.createRenderPass(vk::RenderPassCreateInfo{ {}, attachments, subpass_desc, dependencies });
}

void VkEngine::init_framebuffers() {
    _framebuffers.resize(_swapchainImages.size());
    for (size_t i = 0; i < _swapchainImages.size(); ++i) {
        const std::array<vk::ImageView, 2> attachments{ _swapchainImageViews[i], _depthImageView };
        _framebuffers[i] = _device.createFramebuffer(vk::FramebufferCreateInfo{ {}, _renderPass, attachments, _windowExtent.width, _windowExtent.height, 1});
    }
}

void VkEngine::init_pipelines() {
    const std::vector<vk::DescriptorPoolSize> material_pool_sizes{
        vk::DescriptorPoolSize{ vk::DescriptorType::eCombinedImageSampler, num_combined_sampler_buffers },
    };
    _materialDescriptorPool = _device.createDescriptorPool(vk::DescriptorPoolCreateInfo{ {}, max_material_descriptor_sets, material_pool_sizes });

    auto colored_frag_module = compile_shader_module(_device, "shaders/default_lit.frag", shaderc_shader_kind::shaderc_fragment_shader);
    auto textured_frag_module = compile_shader_module(_device, "shaders/textured_lit.frag", shaderc_shader_kind::shaderc_fragment_shader);
    auto mesh_vert_module = compile_shader_module(_device, "shaders/tri_mesh.vert", shaderc_shader_kind::shaderc_vertex_shader);

    VkPipelineBuilder pipeline_builder;
    pipeline_builder._vertexInputInfo = Vertex::VertexInputInfo;
    pipeline_builder._inputAssembly = vk::PipelineInputAssemblyStateCreateInfo{ {}, vk::PrimitiveTopology::eTriangleList, VK_FALSE };
    pipeline_builder._viewport = vk::Viewport{ 0.0f, 0.0f, (float) _windowExtent.width, (float) _windowExtent.height, 0.0f, 1.0f };
    pipeline_builder._scissor = vk::Rect2D{ vk::Offset2D{ 0, 0 }, _windowExtent };
    pipeline_builder._rasterizer = vkinit::rasterization_state_create_info(vk::PolygonMode::eFill);
    pipeline_builder._multisampling = vkinit::multisample_state_create_info(vk::SampleCountFlagBits::e1);
    pipeline_builder._colorBlendAttachment = vkinit::color_blend_attachment_state();
    pipeline_builder._depthStencil = vkinit::depth_stencil_create_info(true, true, vk::CompareOp::eLessOrEqual);
    pipeline_builder._shaderStages = {
        vkinit::shader_stage_create_info(vk::ShaderStageFlagBits::eVertex, mesh_vert_module),
        vkinit::shader_stage_create_info(vk::ShaderStageFlagBits::eFragment, colored_frag_module)
    };

    {
        const std::array<vk::DescriptorSetLayout, 2> layouts{ _globalDescriptorSetLayout, _objectDescriptorSetLayout };
        pipeline_builder._pipelineLayout = _device.createPipelineLayout(vk::PipelineLayoutCreateInfo{ {}, layouts, nullptr });

        auto default_pipeline = pipeline_builder.build(_device, _renderPass);
        _materials["default_mesh"] = Material{ default_pipeline, pipeline_builder._pipelineLayout, VK_NULL_HANDLE };
    }

    {
        const std::array<vk::DescriptorSetLayout, 3> layouts{ _globalDescriptorSetLayout, _objectDescriptorSetLayout, _singleTextureSetLayout };
        pipeline_builder._pipelineLayout = _device.createPipelineLayout(vk::PipelineLayoutCreateInfo{ {}, layouts, nullptr });

        pipeline_builder._shaderStages = {
            vkinit::shader_stage_create_info(vk::ShaderStageFlagBits::eVertex, mesh_vert_module),
            vkinit::shader_stage_create_info(vk::ShaderStageFlagBits::eFragment, textured_frag_module)
        };
        auto textured_pipeline = pipeline_builder.build(_device, _renderPass);

        const auto texture_set = _device.allocateDescriptorSets(vk::DescriptorSetAllocateInfo{ _materialDescriptorPool, _singleTextureSetLayout }).front();

        Texture& texture = _loaded_textures.at("empire_diffuse");

        const vk::DescriptorImageInfo image_buffer_info{ nearest_sampler, texture.image_view, vk::ImageLayout::eShaderReadOnlyOptimal };
        const vk::WriteDescriptorSet write = vkinit::write_descriptor_image(texture_set, combined_sampler_binding,
            vk::DescriptorType::eCombinedImageSampler, image_buffer_info);
        _device.updateDescriptorSets(write, nullptr);

        _materials["textured_mesh"] = Material{ textured_pipeline, pipeline_builder._pipelineLayout, texture_set };
    }

    _device.destroyShaderModule(colored_frag_module);
    _device.destroyShaderModule(textured_frag_module);
    _device.destroyShaderModule(mesh_vert_module);
}

void VkEngine::load_textures() {
    static const vk::Format image_format = vk::Format::eR8G8B8A8Srgb;
    const auto format_props = _physicalDevice.getFormatProperties(image_format);
    static const auto required_format_feature_flags = vk::FormatFeatureFlagBits::eTransferDst | vk::FormatFeatureFlagBits::eBlitDst | vk::FormatFeatureFlagBits::eBlitSrc | vk::FormatFeatureFlagBits::eSampledImageFilterLinear;
    if ((format_props.optimalTilingFeatures & required_format_feature_flags) != required_format_feature_flags)
        throw std::runtime_error{ "texture image format does not support features required to create texture (with mipmaps)." };

    Texture lost_empire;
    lost_empire.image = vkutil::load_image_from_file(*this, "assets/lost_empire-RGBA.png", image_format);
    lost_empire.image_view = _device.createImageView(vkinit::imageview_create_info(image_format, lost_empire.image._image, vk::ImageAspectFlagBits::eColor));

    _loaded_textures["empire_diffuse"] = lost_empire;
}

void VkEngine::load_meshes() {
    //Mesh triangle_mesh;
    //triangle_mesh._vertices = {
    //    //      positions:            normals:              colors:
    //    Vertex{ { 1.0f, 1.0f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f } },
    //    Vertex{ {-1.0f, 1.0f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f } },
    //    Vertex{ { 0.0f,-1.0f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f } }
    //};
    //upload_mesh(triangle_mesh);
    //_meshes["triangle"] = std::move(triangle_mesh);

    //Mesh monkey_mesh;
    //monkey_mesh.load_model("assets/monkey_smooth.obj");
    //upload_mesh(monkey_mesh);
    //_meshes["monkey"] = std::move(monkey_mesh);

    Mesh lost_empire_mesh;
    lost_empire_mesh.load_model("assets/lost_empire.obj");
    upload_mesh(lost_empire_mesh);
    _meshes["lost_empire"] = std::move(lost_empire_mesh);
}

AllocatedBuffer VkEngine::create_buffer(vk::DeviceSize size, vk::BufferUsageFlags usage, VmaMemoryUsage memory_usage, VmaAllocationCreateFlags allocation_flags, vk::MemoryPropertyFlags required_flags) {
    const vk::BufferCreateInfo buffer_info{ {}, size, usage, vk::SharingMode::eExclusive, nullptr };

    VmaAllocationCreateInfo vma_alloc_info{};
    vma_alloc_info.usage = memory_usage;
    vma_alloc_info.flags = allocation_flags;
    vma_alloc_info.requiredFlags = (VkMemoryPropertyFlags) required_flags;

    AllocatedBuffer new_buffer;
    VK_ASSERT(vmaCreateBuffer(_allocator, &(VkBufferCreateInfo&) buffer_info, &vma_alloc_info, &(VkBuffer&) new_buffer._buffer, &new_buffer._allocation, nullptr), "failed to create buffer.");

    return new_buffer;
}

void VkEngine::upload_mesh(Mesh& mesh) {
    const size_t buffer_size = mesh._vertices.size() * sizeof(Vertex);
    AllocatedBuffer staging_buffer = create_buffer(buffer_size, vk::BufferUsageFlagBits::eTransferSrc, VMA_MEMORY_USAGE_AUTO_PREFER_HOST, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    void* data;
    vmaMapMemory(_allocator, staging_buffer._allocation, &data);
    std::memcpy(data, mesh._vertices.data(), buffer_size);
    vmaUnmapMemory(_allocator, staging_buffer._allocation);

    const vk::BufferCreateInfo vertex_buffer_info{ {}, buffer_size, vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst, vk::SharingMode::eExclusive, nullptr };

    VmaAllocationCreateInfo vma_alloc_info{};
    vma_alloc_info.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;
    vma_alloc_info.preferredFlags = (VkMemoryPropertyFlags) vk::MemoryPropertyFlagBits::eDeviceLocal;

    VK_ASSERT(vmaCreateBuffer(_allocator, &(VkBufferCreateInfo&) vertex_buffer_info, &vma_alloc_info, &(VkBuffer&) mesh._vertexBuffer._buffer, &mesh._vertexBuffer._allocation, nullptr), "failed to create buffer.");

    begin_submit();
    _uploadContext._commandBuffer.copyBuffer(staging_buffer._buffer, mesh._vertexBuffer._buffer, { vk::BufferCopy{ 0, 0, buffer_size } });
    end_submit();

    vmaDestroyBuffer(_allocator, staging_buffer._buffer, staging_buffer._allocation);
}

void VkEngine::init_scene() {
    //_renderables.push_back(RenderObject{ &_meshes.at("monkey"), &_materials.at("default_mesh"), glm::mat4(1.0f) });

    //_renderables.reserve(_renderables.size() + 41 * 41);
    //for (int x = -20; x <= 20; ++x) {
    //    for (int y = -20; y <= 20; ++y) {
    //        glm::mat4 translation = glm::translate(glm::mat4(1.0f), glm::vec3{ x, 0, y });
    //        glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3{ 0.2f, 0.2f, 0.2f });
    //        _renderables.push_back(RenderObject{ &_meshes.at("triangle"), &_materials.at("default_mesh"), translation * scale});
    //    }
    //}

    _renderables.push_back(RenderObject{ &_meshes.at("lost_empire"), &_materials.at("textured_mesh"), glm::translate(glm::mat4(1.0f), glm::vec3{ 5,-10,0 }) });

    ASSERT(_renderables.size() < max_objects, "number of renderable objects exceeds the max supported object count of the renderer.");
}

void VkEngine::begin_submit() {
    _uploadContext._commandBuffer.begin(vk::CommandBufferBeginInfo{ vk::CommandBufferUsageFlagBits::eOneTimeSubmit, nullptr });
}

void VkEngine::end_submit() {
    _uploadContext._commandBuffer.end();
    _graphicsQueue.submit(vk::SubmitInfo{ nullptr, nullptr, _uploadContext._commandBuffer, nullptr }, _uploadContext._uploadFence);
    VK_ASSERT(_device.waitForFences(_uploadContext._uploadFence, VK_TRUE, std::numeric_limits<uint64_t>::max()), "fence timeout");
    _device.resetFences(_uploadContext._uploadFence);
    _device.resetCommandPool(_uploadContext._commandPool);
}

void VkEngine::cleanup() {
    cleanup_swapchain();

    // load_textures()
    for (const auto& [name, texture] : _loaded_textures) {
        vmaDestroyImage(_allocator, texture.image._image, texture.image._allocation);
        _device.destroyImageView(texture.image_view);
    }

    // load_meshes()
    for (const auto& [name, mesh] : _meshes) {
        vmaDestroyBuffer(_allocator, mesh._vertexBuffer._buffer, mesh._vertexBuffer._allocation);
    }

    // init_descriptors()
    _device.destroyDescriptorSetLayout(_objectDescriptorSetLayout);
    _device.destroyDescriptorSetLayout(_globalDescriptorSetLayout);
    _device.destroyDescriptorPool(_globalDescriptorPool);
    _device.destroySampler(nearest_sampler);

    // init_vulkan()
    _device.destroyCommandPool(_uploadContext._commandPool);
    _device.destroyFence(_uploadContext._uploadFence);
    vmaDestroyBuffer(_allocator, _sceneDataBuffer._buffer, _sceneDataBuffer._allocation);
    for (const auto& frame_datum : _frameData) {
        _device.destroyFence(frame_datum._renderFence);
        _device.destroySemaphore(frame_datum._presentSemaphore);
        _device.destroySemaphore(frame_datum._renderSemaphore);

        _device.destroyCommandPool(frame_datum._commandPool);

        vmaDestroyBuffer(_allocator, frame_datum._uboCamera._buffer, frame_datum._uboCamera._allocation);
        vmaDestroyBuffer(_allocator, frame_datum._ssboObjects._buffer, frame_datum._ssboObjects._allocation);
    }
    vmaDestroyAllocator(_allocator);
    vkb::destroy_device(_vkbDevice);
    _instance.destroySurfaceKHR(_surface);
    vkb::destroy_instance(_vkbInstance);

    glfwDestroyWindow(_window);

    glfwTerminate();
}

void VkEngine::cleanup_swapchain() {
    _device.waitIdle();

    if (!_swapchain)
        return;

    // init_pipelines()
    // note: do NOT clear the _materials container, otherwise _renderables will be using invalidated pointers ( after init_swapchain() ).
    for (const auto& [name, material] : _materials) {
        _device.destroyPipelineLayout(material.pipeline_layout);
        _device.destroyPipeline(material.pipeline);
    }
    _device.destroyDescriptorPool(_materialDescriptorPool);

    // init_framebuffers()
    for (size_t i = 0; i < _swapchainImages.size(); ++i) {
        _device.destroyFramebuffer(_framebuffers[i]);
    }

    // init_default_renderpass()
    _device.destroyRenderPass(_renderPass);

    // init_swapchain()
    vkb::destroy_swapchain(_vkbSwapchain);
    _swapchain = nullptr;
    for (const vk::ImageView& image_view : _swapchainImageViews) {
        _device.destroyImageView(image_view);
    }
    vmaDestroyImage(_allocator, (VkImage&) _depthImage._image, _depthImage._allocation);
    _device.destroyImageView(_depthImageView);
}

void VkEngine::draw() {
    const FrameData& frame_datum = _frameData[_frameDataIdx];

    VK_ASSERT(_device.waitForFences(frame_datum._renderFence, VK_TRUE, std::numeric_limits<uint64_t>::max()), "fence timeout");
    _device.resetFences(frame_datum._renderFence);

    // locks FPS using acquireNextImageKHR()
    
    auto reacquire_swapchain_and_image = [this, &frame_datum]() -> uint32_t {
        cleanup_swapchain();
        init_swapchain();

        auto acquire_result = _device.acquireNextImageKHR(_swapchain, std::numeric_limits<uint64_t>::max(), frame_datum._presentSemaphore, VK_NULL_HANDLE);
        VK_ASSERT(acquire_result.result, "unable to acquire swapchain image.");

        return acquire_result.value;
    };

    uint32_t image_idx;

    if (_isSwapchainDirty) {
        _isSwapchainDirty = false;
        image_idx = reacquire_swapchain_and_image();
    }
    else {
        auto acquire_result = _device.acquireNextImageKHR(_swapchain, std::numeric_limits<uint64_t>::max(), frame_datum._presentSemaphore, VK_NULL_HANDLE);
        switch (acquire_result.result) {
        case vk::Result::eSuccess:
            image_idx = acquire_result.value;
            break;
        case vk::Result::eSuboptimalKHR:
        case vk::Result::eErrorOutOfDateKHR:
            image_idx = reacquire_swapchain_and_image();
            break;
        default:
            throw std::runtime_error{ "unable to acquire swapchain image." };
        }
    }

    frame_datum._mainCommandBuffer.reset();
    frame_datum._mainCommandBuffer.begin(vk::CommandBufferBeginInfo{ vk::CommandBufferUsageFlagBits::eOneTimeSubmit, nullptr });
    const vk::ClearColorValue clear_color{ std::array<float, 4>{ 0.0f, 0.0f, 0.4f, 1.0f } };
    const vk::ClearDepthStencilValue clear_depth{ 1.0f, 0 };
    const std::array<vk::ClearValue, 2> clear_values{ clear_color, clear_depth };
    frame_datum._mainCommandBuffer.beginRenderPass(vk::RenderPassBeginInfo{ _renderPass, _framebuffers[image_idx], vk::Rect2D{ vk::Offset2D{ 0, 0 }, _windowExtent }, clear_values }, vk::SubpassContents::eInline);

    if(!std::is_sorted(_renderables.begin(), _renderables.end()))
        std::sort(_renderables.begin(), _renderables.end());

    draw_objects(frame_datum, _renderables.data(), _renderables.size());

    frame_datum._mainCommandBuffer.endRenderPass();
    frame_datum._mainCommandBuffer.end();

    const std::array<vk::PipelineStageFlags, 1> wait_stages{ vk::PipelineStageFlagBits::eColorAttachmentOutput };
    const vk::SubmitInfo submit_info{ frame_datum._presentSemaphore, wait_stages, frame_datum._mainCommandBuffer, frame_datum._renderSemaphore };
    _graphicsQueue.submit(submit_info, frame_datum._renderFence);

    VK_ASSERT(_graphicsQueue.presentKHR(vk::PresentInfoKHR{ frame_datum._renderSemaphore, _swapchain, image_idx, nullptr }), "presentation failed");

    _frameDataIdx = (_frameDataIdx + 1) % max_frames_in_flight;
    ++_frameNumber;
}

void VkEngine::draw_objects(const FrameData& frame_datum, const RenderObject* first, size_t count) {
    const glm::vec3 cam_pos{ 0.0f, -6.0f, -10.0f };
    const glm::mat4 view = glm::translate(glm::mat4(1.0f), cam_pos);
    glm::mat4 projection = glm::perspective(glm::radians(70.0f), _windowExtent.width / (float) _windowExtent.height, 0.1f, 200.0f);
    projection[1][1] *= -1.0f; // invert the Y axis.

    const Mesh* previous_mesh = nullptr;
    const Material* previous_material = nullptr;

    UboCameraData ubo_camera;
    ubo_camera.proj = projection;
    ubo_camera.view = view;
    ubo_camera.viewproj = projection * view;

    void* camera_data;
    vmaMapMemory(_allocator, frame_datum._uboCamera._allocation, &camera_data);
    std::memcpy(camera_data, &ubo_camera, sizeof(UboCameraData));
    vmaUnmapMemory(_allocator, frame_datum._uboCamera._allocation);

    _uboSceneData.ambient_color = glm::vec4{ std::sin(_frameNumber / 120.0f), 0.0f, std::cos(_frameNumber / 120.0f), 1.0f };

    std::byte* scene_data;
    vmaMapMemory(_allocator, _sceneDataBuffer._allocation, reinterpret_cast<void**>(&scene_data));
    scene_data += pad_uniform_buffer_size(sizeof(UboSceneData)) * _frameDataIdx;
    std::memcpy(scene_data, &_uboSceneData, sizeof(UboSceneData));
    vmaUnmapMemory(_allocator, _sceneDataBuffer._allocation);

    SsboObjectData* object_data;
    vmaMapMemory(_allocator, frame_datum._ssboObjects._allocation, reinterpret_cast<void**>(&object_data));
    for (size_t i = 0; i < count; ++i) {
        const RenderObject& object = first[i];
        object_data[i].model_matrix = object.transform_mat;
    }
    vmaUnmapMemory(_allocator, frame_datum._ssboObjects._allocation);

    for (size_t i = 0; i < count; ++i) {
        const RenderObject& object = first[i];

        if (object.material != previous_material) {
            frame_datum._mainCommandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, object.material->pipeline);
            const uint32_t uniform_offset = pad_uniform_buffer_size(sizeof(UboSceneData)) * _frameDataIdx;
            frame_datum._mainCommandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, object.material->pipeline_layout,
                0, { _frameGlobalDescriptors[_frameDataIdx], _frameObjectDescriptors[_frameDataIdx] }, { uniform_offset });
            if (object.material->texture_set) {
                frame_datum._mainCommandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, object.material->pipeline_layout,
                    2, { object.material->texture_set }, nullptr);
            }
            previous_material = object.material;
        }
        
        if (object.mesh != previous_mesh) {
            const std::array<vk::DeviceSize, 1> offsets{ 0 };
            frame_datum._mainCommandBuffer.bindVertexBuffers(0, object.mesh->_vertexBuffer._buffer, offsets);
            previous_mesh = object.mesh;
        }

        frame_datum._mainCommandBuffer.draw(object.mesh->_vertices.size(), 1, 0, i);
    }
}

void VkEngine::run() {
    while (!glfwWindowShouldClose(_window)) {
        glfwPollEvents();
        if (_lifecycleState.has_graphics) {
            if (_lifecycleState.has_input_focus)
                draw();
            else
                glfwWaitEvents();
        }
        else {
            cleanup_swapchain();
            glfwWaitEvents();
        }
    }
}
