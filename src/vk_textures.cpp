#include "vk_textures.h"
#include "vk_initializers.h"
#include "util.h"

#include <stb_image.h>

namespace vkutil {
    AllocatedImage load_image_from_file(VkEngine& engine, const char* file_path, vk::Format image_format) {
        int tex_width, tex_height, num_channels;
        stbi_uc* pixels = stbi_load(file_path, &tex_width, &tex_height, &num_channels, STBI_rgb_alpha);
        
        if (!pixels)
            throw std::runtime_error{ "failed to load texture image." };

        const vk::DeviceSize image_size = tex_width * tex_height * 4;
        AllocatedBuffer staging_buffer = engine.create_buffer(image_size, vk::BufferUsageFlagBits::eTransferSrc, VMA_MEMORY_USAGE_AUTO_PREFER_HOST, VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
        void* data;
        vmaMapMemory(engine._allocator, staging_buffer._allocation, &data);
        std::memcpy(data, pixels, (size_t) image_size);
        vmaUnmapMemory(engine._allocator, staging_buffer._allocation);

        stbi_image_free(pixels);

        const vk::Extent3D extent{ (uint32_t) tex_width, (uint32_t) tex_height, 1 };

        const vk::ImageCreateInfo image_info = vkinit::image_create_info(image_format, extent, 1, vk::SampleCountFlagBits::e1, vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferDst);
        AllocatedImage new_image;

        VmaAllocationCreateInfo vma_alloc_info{};
        vma_alloc_info.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;
        vma_alloc_info.preferredFlags = (VkMemoryPropertyFlags) vk::MemoryPropertyFlagBits::eDeviceLocal;

        VK_ASSERT(vmaCreateImage(engine._allocator, &(const VkImageCreateInfo&) image_info, &vma_alloc_info, &(VkImage&) new_image._image, &new_image._allocation, nullptr), "failed to create image.");

        engine.begin_submit();
        const vk::ImageSubresourceRange range{ vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 };
        const vk::ImageMemoryBarrier imagebarrier_to_transfer{ {}, vk::AccessFlagBits::eTransferWrite, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, new_image._image, range };
        engine._uploadContext._commandBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer, {}, nullptr, nullptr, imagebarrier_to_transfer);
        vk::BufferImageCopy copy_info{ 0, 0, 0, vk::ImageSubresourceLayers{ vk::ImageAspectFlagBits::eColor, 0, 0, 1 }, vk::Offset3D{ 0, 0, 0 }, extent };
        engine._uploadContext._commandBuffer.copyBufferToImage(staging_buffer._buffer, new_image._image, vk::ImageLayout::eTransferDstOptimal, copy_info);
        const vk::ImageMemoryBarrier imagebarrier_to_shader_read{ vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eShaderRead, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal, VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, new_image._image, range };
        engine._uploadContext._commandBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, {}, nullptr, nullptr, imagebarrier_to_shader_read);
        engine.end_submit();

        vmaDestroyBuffer(engine._allocator, staging_buffer._buffer, staging_buffer._allocation);

        return new_image;
	}
}
