#include "shader_compile.h"

#include <fstream>
#include <iostream>

const char* to_string(shaderc_compilation_status status) {
    switch (status) {
    case shaderc_compilation_status_success:
        return "success";
    case shaderc_compilation_status_invalid_stage:
        return "invalid_stage";
    case shaderc_compilation_status_compilation_error:
        return "compilation_error";
    case shaderc_compilation_status_internal_error:
        return "internal_error";
    case shaderc_compilation_status_null_result_object:
        return "null_result_object";
    case shaderc_compilation_status_invalid_assembly:
        return "invalid_assembly";
    case shaderc_compilation_status_validation_error:
        return "validation_error";
    case shaderc_compilation_status_transformation_error:
        return "transformation_error";
    case shaderc_compilation_status_configuration_error:
        return "configuration_error";
    default:
        throw std::logic_error{ "unrecognised value" };
    }
}

std::string load_shader_from_file(const char* path) {
    std::string shader_source;

    std::ifstream shader_file;
    shader_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        shader_file.open(path, std::ios::ate | std::ios::binary);
        size_t file_size = shader_file.tellg();
        shader_source.resize(file_size);
        shader_file.seekg(0);
        shader_file.read(shader_source.data(), file_size);
        shader_file.close();
    }
    catch (std::ifstream::failure& e) {
        throw std::runtime_error{ std::string("unable to read shader file: ") + path };
    }

    return shader_source;
}

shaderc::SpvCompilationResult compile_glsl_to_spv(const std::string& shader_source, shaderc_shader_kind shader_kind, const char* input_file_name) {
    shaderc::Compiler compiler{};
    shaderc::CompileOptions compile_options{};
    compile_options.SetOptimizationLevel(shaderc_optimization_level_performance);
    auto spv = compiler.CompileGlslToSpv(shader_source, shader_kind, input_file_name, compile_options);
    if (spv.GetCompilationStatus() != shaderc_compilation_status_success) {
        std::cerr << "failed: " << to_string(spv.GetCompilationStatus()) << " - errors: " << spv.GetNumErrors() << " warnings: " << spv.GetNumWarnings() << std::endl;
        std::cerr << spv.GetErrorMessage() << std::endl;
        throw std::runtime_error{ "unable to compile shader." };
    }

    return spv;
}

vk::ShaderModule compile_shader_module(vk::Device device, const char* path, shaderc_shader_kind shader_kind) {
    std::string shader_source = load_shader_from_file(path);
    auto spv = compile_glsl_to_spv(shader_source, shader_kind, path);
    return device.createShaderModule(vk::ShaderModuleCreateInfo{ {}, sizeof(shaderc::SpvCompilationResult::element_type) * static_cast<size_t>(spv.end() - spv.begin()), spv.begin() });
}
