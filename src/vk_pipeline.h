#pragma once

#include <vk_types.h>
#include <vector>

class VkPipelineBuilder {
public:
	std::vector<vk::PipelineShaderStageCreateInfo> _shaderStages;
	vk::PipelineVertexInputStateCreateInfo _vertexInputInfo;
	vk::PipelineInputAssemblyStateCreateInfo _inputAssembly;
	vk::Viewport _viewport;
	vk::Rect2D _scissor;
	vk::PipelineRasterizationStateCreateInfo _rasterizer;
	vk::PipelineColorBlendAttachmentState _colorBlendAttachment;
	vk::PipelineMultisampleStateCreateInfo _multisampling;
	vk::PipelineLayout _pipelineLayout;
	vk::PipelineDepthStencilStateCreateInfo _depthStencil;

	vk::Pipeline build(vk::Device device, vk::RenderPass renderpass);
};
