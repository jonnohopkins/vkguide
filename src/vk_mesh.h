#pragma once

#include "vk_types.h"
#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/matrix.hpp>

struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec3 color;
	glm::vec2 uv;

	static const vk::VertexInputBindingDescription BindingDescription;
	static const std::array<vk::VertexInputAttributeDescription, 4> AttributeDescriptions;
	static const vk::PipelineVertexInputStateCreateInfo VertexInputInfo;
};

struct Mesh {
	std::vector<Vertex> _vertices;
	AllocatedBuffer _vertexBuffer; // owning

	void load_model(const char* path);
};

struct MeshPushConstants {
	glm::vec4 data;
	glm::mat4 render_matrix;
};
