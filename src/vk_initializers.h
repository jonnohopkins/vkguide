#pragma once

#include <vk_types.h>

namespace vkinit {
	inline vk::PipelineShaderStageCreateInfo shader_stage_create_info(vk::ShaderStageFlagBits stage, vk::ShaderModule shader_module) {
		return vk::PipelineShaderStageCreateInfo{ {}, stage, shader_module, "main" };
	}

	inline vk::PipelineRasterizationStateCreateInfo rasterization_state_create_info(vk::PolygonMode polygon_mode) {
		return vk::PipelineRasterizationStateCreateInfo{ {}, VK_FALSE, VK_FALSE,
			polygon_mode,
			vk::CullModeFlagBits::eNone, vk::FrontFace::eClockwise,
			VK_FALSE, 0.0f, 0.0f, 0.0f, 1.0f
		};
	}

	inline vk::PipelineMultisampleStateCreateInfo multisample_state_create_info(vk::SampleCountFlagBits msaa_samples) {
		return vk::PipelineMultisampleStateCreateInfo{ {}, msaa_samples, VK_FALSE, 1.0f, nullptr, VK_FALSE, VK_FALSE };
	}

	inline vk::PipelineColorBlendAttachmentState color_blend_attachment_state() {
		return vk::PipelineColorBlendAttachmentState{
			VK_FALSE,
			vk::BlendFactor::eZero, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
			vk::BlendFactor::eZero, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
			vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA
		};
	}

	inline vk::ImageCreateInfo image_create_info(vk::Format format, vk::Extent3D extent, uint32_t mip_levels, vk::SampleCountFlagBits samples, vk::ImageUsageFlags usage) {
		return vk::ImageCreateInfo{ {}, vk::ImageType::e2D, format, extent, mip_levels, 1, samples, vk::ImageTiling::eOptimal,
			usage, vk::SharingMode::eExclusive, nullptr, vk::ImageLayout::eUndefined };
	}

	inline vk::ImageViewCreateInfo imageview_create_info(vk::Format format, vk::Image image, vk::ImageAspectFlags aspect) {
		return vk::ImageViewCreateInfo{ {}, image, vk::ImageViewType::e2D, format, {}, vk::ImageSubresourceRange{ aspect, 0, 1, 0, 1 } };
	}

	inline vk::PipelineDepthStencilStateCreateInfo depth_stencil_create_info(bool depth_test, bool depth_write, vk::CompareOp compare_op) {
		return vk::PipelineDepthStencilStateCreateInfo{
			{}, depth_test, depth_write, compare_op,
			VK_FALSE, VK_FALSE, {}, {}, 0.0f, 1.0f
		};
	}

	inline vk::DescriptorSetLayoutBinding descriptorset_layout_binding(vk::DescriptorType type, vk::ShaderStageFlags stage_flags, uint32_t binding) {
		return vk::DescriptorSetLayoutBinding{ binding, type, 1, stage_flags, nullptr };
	}

	inline vk::WriteDescriptorSet write_descriptor_set(vk::DescriptorType type, vk::DescriptorSet dst_set, const vk::DescriptorBufferInfo* buffer_info, uint32_t binding) {
		return vk::WriteDescriptorSet{ dst_set, binding, 0, 1, type, nullptr, buffer_info, nullptr };
	}

	inline vk::SamplerCreateInfo sampler_create_info(vk::Filter filters, vk::SamplerAddressMode sampler_address_mode) {
		return vk::SamplerCreateInfo{ {}, filters, filters,
			vk::SamplerMipmapMode::eNearest, sampler_address_mode, sampler_address_mode, sampler_address_mode,
			0.0f, VK_FALSE, 0.0f, VK_FALSE, vk::CompareOp::eNever, 0.0f, 0.0f, vk::BorderColor::eFloatTransparentBlack, VK_FALSE };
	}

	inline vk::WriteDescriptorSet write_descriptor_image(vk::DescriptorSet dst_set, uint32_t binding, vk::DescriptorType type, vk::DescriptorImageInfo image_info) {
		return vk::WriteDescriptorSet{ dst_set, binding, 0, type, image_info, nullptr, nullptr };
	}
}