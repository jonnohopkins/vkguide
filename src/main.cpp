#include <vk_engine.h>

int main(int argc, const char** arv) {
	VkEngine engine;
	engine.init();
	engine.run();
	engine.cleanup();

	return 0;
}