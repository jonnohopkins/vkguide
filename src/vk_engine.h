#pragma once

#include "vk_types.h"
#include "vk_mesh.h"
#include <vector>
#include <VkBootstrap.h>
#include <vk_mem_alloc.h>
#include <unordered_map>

struct Material {
	vk::Pipeline pipeline; // owning
	vk::PipelineLayout pipeline_layout; // owning
	vk::DescriptorSet texture_set; // owning, may be VK_NULL_HANDLE
};

struct Texture {
	AllocatedImage image;
	vk::ImageView image_view;
};

struct RenderObject {
	const Mesh* mesh; // owned by VkEngine
	const Material* material; // owned by VkEngine
	glm::mat4 transform_mat;

	inline friend bool operator <(const RenderObject& lhs, const RenderObject& rhs) {
		return lhs.material->pipeline < rhs.material->pipeline || (lhs.material->pipeline == rhs.material->pipeline && lhs.mesh < rhs.mesh);
	}
};

struct LifecycleState {
	// true does not imply that the app has exclusive input (ie. other apps may receive some input events).
	// false does not imply that the app won't receive some input events.
	bool has_input_focus{ false };
	// must not render while false
	bool has_graphics{ false };
};

class VkEngine {
public:
	struct UboCameraData {
		alignas(16) glm::mat4 view;
		alignas(16) glm::mat4 proj;
		alignas(16) glm::mat4 viewproj;
	};

	struct UboSceneData {
		glm::vec4 fog_color; // w is for exponent
		glm::vec4 fog_distances; // x for min, y for max, zw unused.
		glm::vec4 ambient_color;
		glm::vec4 sunlight_direction; // w for sun power
		glm::vec4 sunlight_color;
	};

	struct SsboObjectData {
		glm::mat4 model_matrix;
	};

	struct FrameData {
		vk::Semaphore _presentSemaphore, _renderSemaphore;
		vk::Fence _renderFence;
		vk::CommandPool _commandPool;
		vk::CommandBuffer _mainCommandBuffer;
		AllocatedBuffer _uboCamera;
		AllocatedBuffer _ssboObjects;
	};

	struct UploadContext {
		vk::Fence _uploadFence;
		vk::CommandPool _commandPool;
		vk::CommandBuffer _commandBuffer;
	};

	static constexpr size_t max_frames_in_flight = 2;
	static constexpr size_t num_uniform_buffers = max_frames_in_flight;
	static constexpr size_t num_uniform_dynamic_buffers = max_frames_in_flight;
	static constexpr size_t num_storage_buffers = 1;
	static constexpr size_t max_global_descriptor_sets = 2 * max_frames_in_flight;
	static constexpr size_t num_combined_sampler_buffers = 1;
	static constexpr size_t max_material_descriptor_sets = 1;
	static constexpr size_t max_objects = 4 * 1024;

	LifecycleState _lifecycleState;
	unsigned int _frameNumber{ 0 };
	unsigned int _frameDataIdx{ 0 };

	struct GLFWwindow* _window{ nullptr };
	vk::Extent2D _windowExtent;

	vkb::Instance _vkbInstance;
	vk::Instance _instance;
	vk::DebugUtilsMessengerEXT _debugMessenger;
	vk::PhysicalDevice _physicalDevice;
	vk::PhysicalDeviceProperties _physicalDeviceProperties;
	vkb::Device _vkbDevice;
	vk::Device _device;
	vk::SurfaceKHR _surface;
	vk::Queue _graphicsQueue;
	uint32_t _graphicsQueueFamily;
	VmaAllocator _allocator;
	std::vector<FrameData> _frameData; // size is max_frames_in_flight
	vk::DescriptorPool _globalDescriptorPool;
	vk::DescriptorPool _materialDescriptorPool;
	vk::DescriptorSetLayout _globalDescriptorSetLayout;
	std::vector<vk::DescriptorSet> _frameGlobalDescriptors; // per frame in flight
	vk::DescriptorSetLayout _objectDescriptorSetLayout;
	std::vector<vk::DescriptorSet> _frameObjectDescriptors; // per frame in flight
	UboSceneData _uboSceneData;
	AllocatedBuffer _sceneDataBuffer;
	vk::DescriptorSetLayout _singleTextureSetLayout;
	vk::Sampler nearest_sampler;
	UploadContext _uploadContext;

	// swapchain dependant fields:
	bool _isSwapchainDirty{ false };
	vkb::Swapchain _vkbSwapchain;
	vk::SwapchainKHR _swapchain;
	vk::Format _swapchainImageFormat;
	std::vector<vk::Image> _swapchainImages; // per swapchain
	std::vector<vk::ImageView> _swapchainImageViews; // per swapchain
	std::vector<vk::Framebuffer> _framebuffers; // per swapchain
	vk::RenderPass _renderPass;
	vk::Format _depthFormat;
	AllocatedImage _depthImage;
	vk::ImageView _depthImageView;

	std::vector<RenderObject> _renderables; // references to _materials and _meshes remain valid as long as no clear, insert or erase occurs.
	std::unordered_map<std::string, Material> _materials;
	std::unordered_map<std::string, Mesh> _meshes;
	std::unordered_map<std::string, Texture> _loaded_textures;

	// set 0:
	static constexpr uint32_t ubo_camera_binding = 0;
	static constexpr uint32_t ubo_scene_binding = 1;
	// set 1:
	static constexpr uint32_t ssbo_object_binding = 0;
	// set 2:
	static constexpr uint32_t combined_sampler_binding = 0;

	void init();
	void init_vulkan();
	void init_descriptors(); // ie. _globalDescriptorPool descriptors
	void init_swapchain();
	void init_default_renderpass(); // needs to be recreated along with the swapchain
	void init_framebuffers(); // needs to be recreated along with the swapchain
	// needs to be recreated along with the swapchain
	// creates the materials and _materialDescriptorPool descriptors as well
	void init_pipelines();
	void load_textures();
	void load_meshes();
	void upload_mesh(Mesh& mesh);
	void init_scene();
	void cleanup();
	void cleanup_swapchain();

	AllocatedBuffer create_buffer(vk::DeviceSize size, vk::BufferUsageFlags usage, VmaMemoryUsage memory_usage, VmaAllocationCreateFlags allocation_flags, vk::MemoryPropertyFlags required_flags);

	inline size_t pad_uniform_buffer_size(size_t original_size) {
		const size_t min_ubo_alignment = _physicalDeviceProperties.limits.minUniformBufferOffsetAlignment;
		return (min_ubo_alignment > 0) ? ((original_size + min_ubo_alignment - 1) & ~(min_ubo_alignment - 1))
			: original_size;
	}

	void begin_submit();
	void end_submit();

	void draw();
	void draw_objects(const FrameData& frame_datum, const RenderObject* first, size_t count);

	// the main loop
	void run();
};