#pragma once

#include <vk_types.h>
#include <vk_engine.h>

namespace vkutil {
	// physicalDevice.getFormatProperties(image_format).optimalTilingFeatures must support:
	// (vkCmdCopyBufferToImage:) vk::FormatFeatureFlagBits::eTransferDst |
	// (vkCmdBlitImage:) vk::FormatFeatureFlagBits::eBlitDst | vk::FormatFeatureFlagBits::eBlitSrc | vk::FormatFeatureFlagBits::eSampledImageFilterLinear
	AllocatedImage load_image_from_file(VkEngine& engine, const char* file_path, vk::Format image_format);
}
