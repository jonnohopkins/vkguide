## About

Main differences with the tutorial's source code:

- uses the Vulkan Hpp C++ bindings
- uses GLFW instead of SDL

## Dependencies

Manual:

 - [CMake](https://cmake.org/download/)
 - [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/)

Managed by CMake:

 - [GLFW](https://github.com/glfw/glfw)
 - [GLM](https://github.com/g-truc/glm)
 - [VK-Bootstrap](https://github.com/charles-lunarg/vk-bootstrap)
 - [Dear ImGui](https://github.com/ocornut/imgui)
 - [VulkanMemoryAllocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator)

Included in source:

 - [stb_image](https://github.com/nothings/stb)
 - [tinyobjloader](https://github.com/tinyobjloader/tinyobjloader)

## Building

Select a `<dir>` to generate the project files and run:
 ```
cmake -B <dir>			# create the project
cmake --build <dir>		# build the project
cmake --open <dir>		# open the project
```
